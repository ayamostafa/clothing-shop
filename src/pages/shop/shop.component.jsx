import React,{useEffect,useState} from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';

import { fetchCollectionsStartAsync } from '../../redux/shop/shop.actions';
import CollectionsOverviewContainer from '../../components/collections-overview/collections-overview.container';
import CollectionPageContainer from '../collection/collection.container';

const ShopPage = ({ match,fetchCollectionsStartAsync  }) => {

    useEffect(()=>{
        fetchCollectionsStartAsync();
    },[]);
    return(
        <div className='shop-page'>
            <Route exact path={`${match.path}`}   component={CollectionsOverviewContainer}/>
            <Route path={`${match.path}/:collectionId`}  component={CollectionPageContainer} />
        </div>
    );
}
export default connect(null, {fetchCollectionsStartAsync })(ShopPage);
